﻿using System;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace AutoMapperLib
{
    public static class AutoMapper
    {
        public  static IServiceCollection ConfigureAutomapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MapProfile));
            return services;
        }
    }
    public class MapProfile:Profile
    {

        public MapProfile()
        {
            CreateMap<User, UserDTO>()
            .ReverseMap();
            CreateMap<ArtObject, ArtObjectDTO>()
              .ForMember(s => s.UserId, opt => opt.MapFrom(src => src.user.Id))
              .ReverseMap()
              .ForPath(s => s.user, opt => opt.Ignore());
        }
    }
}

