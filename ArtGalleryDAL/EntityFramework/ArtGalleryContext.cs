﻿using System;
using System.Collections.Generic;
using System.Text;
using ArtGalleryDAL.Entities;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace ArtGalleryDAL.EntityFramework
{
  public  class ArtGalleryContext : DbContext
    {
        
       public ArtGalleryContext(DbContextOptions<ArtGalleryContext> options) 
            : base(options)
        {
            Database.EnsureCreated();
        }
        // DbSet <>talis enq ayn Entity nere voronq map en linum DB um tableneri het
        public DbSet<User> Users { get; set; }
        public DbSet<ArtObject> ArtObjects { get; set; }
         
        //method e kanchvum e erb araji angam mappin e linum depi memory
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //using Fluent API
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>()
                .HasKey(p => p.Id); // add enq anm vorpes key
            modelBuilder.Entity<User>()
                .Property(it => it.Id)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<User>() // usrin talis enq sahmanapakumner
                .Property(prop => prop.Name)
                .IsRequired()
                .HasMaxLength(100);
            modelBuilder.Entity<ArtObject>()
                .HasKey(p => p.Id);
            modelBuilder.Entity<ArtObject>()
                .HasOne(p => p.user)
                .WithMany(p => p.Objects)
                .HasForeignKey(p => p.UserId);
            
        }
    }
}
