﻿using ArtGalleryDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDAL.Entities
{
    public class User : IBaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Account { get; set; }
        protected string LogIn { get; set; }
        protected string Password { get; set; }

        public ICollection<ArtObject> Objects { get; set; }
    }
}
