﻿using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL.Entities;
using ArtGalleryDAL.EntityFramework;
using ArtGalleryDTO.EntitiesDTO;
using AutoMapper;
using System;
using ArtGalleryDAL;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryBLL.ServicesBLLL
{
    public class ArtObjectService : ServiceBase<ArtObject,ArtObjectDTO>, IArtObjectService
    {
        public ArtObjectService(IMapper mapper, ArtGalleryContext context) : base(mapper, context)
        {

        }
    }
}
