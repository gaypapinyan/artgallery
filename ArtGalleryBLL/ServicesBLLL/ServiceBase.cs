﻿using ArtGalleryBLL.InterfacesBLL;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ArtGalleryDAL.Interfaces;
using ArtGalleryDTO.InterfacesDTO;
using AutoMapper;
using ArtGalleryDAL.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ArtGalleryBLL.ServicesBLLL
{
    public class ServiceBase<TEntity, TDto> : IBaseService<TEntity, TDto>
        where TEntity : class, IBaseEntity
        where TDto : class, IBaseEntityDTO
    {
        protected readonly IMapper mapper;
        protected readonly ArtGalleryContext dbContext;
        protected readonly DbSet<TEntity> entities;

        public ServiceBase(IMapper mapper, ArtGalleryContext context)
        {
            this.mapper = mapper;
            this.dbContext = context;
             this.entities = this.dbContext.Set<TEntity>();

        }

        public async Task<TDto> Add(TDto model)
        {
            var toAdd = mapper.Map<TEntity>(model);
            var added = await entities.AddAsync(toAdd);
            await dbContext.SaveChangesAsync();
            var addedentity = added.Entity;
            var addedDto = mapper.Map<TDto>(addedentity);
            return addedDto;

        }

        public async Task<List<TDto>> Get(Expression<Func<TEntity, bool>> expression)
        {
            var querylist = entities.Where(expression);
            var list = await querylist.ToListAsync();
            var listDto = mapper.Map<List<TDto>>(list);
            return listDto;

        }

        public async Task<List<TDto>> GetAll()
        {
            List<TEntity> list = await entities.ToListAsync();
            var listDto = mapper.Map<List<TDto>>(list);
            return listDto;

        }

        public async Task<TDto> GetById(int id)
        {
            var entity = await entities.FindAsync(id);
            var entityDto = mapper.Map<TDto>(entity);
            return entityDto;
        }

        public async Task<TDto> Remove(int id)
        {
            var toRemove = await entities.FindAsync(id);
            var removed = entities.Remove(toRemove);
            await dbContext.SaveChangesAsync();
            var removedEntity = removed.Entity;
            var removedDto = mapper.Map<TDto>(removedEntity);
            return removedDto;
        }

        public void Update(TDto model)
        {
            var newentity = mapper.Map<TEntity>(model);

            var changedentity = entities.Update(newentity);

            dbContext.SaveChangesAsync();
            // var changedDto = mapper.Map<TDto>(changedentity);
            //  return changedDto;

        }

    }
}
