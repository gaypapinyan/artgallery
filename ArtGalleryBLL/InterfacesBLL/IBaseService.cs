﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryBLL.InterfacesBLL
{
    public interface IBaseService<TEntity,TDto>
    {
        Task<TDto> Add(TDto model);
        Task<List<TDto>> GetAll();
        Task<TDto> GetById(int id);
        Task<List<TDto>> Get(Expression<Func<TEntity, bool>> expression);
        Task<TDto> Remove(int id);
        void Update(TDto model);
    }
}
