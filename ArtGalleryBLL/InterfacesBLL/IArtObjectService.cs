﻿using System;
using System.Collections.Generic;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;

namespace ArtGalleryBLL.InterfacesBLL
{
    public interface IArtObjectService : IBaseService<ArtObject,ArtObjectDTO>
    {

    }
}
