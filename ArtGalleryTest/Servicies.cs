﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGalleryBLL.ServicesBLLL;
using ArtGalleryBLL.InterfacesBLL;

namespace ArtGalleryTest
{
    public static class Servicies
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IUserService,UserService>();
            services.AddScoped<IArtObjectService, ArtObjectService>();
           // services.AddScoped<IShoppingCart, ShoppingCartService>();
            return services;
           
        }
    }
}
