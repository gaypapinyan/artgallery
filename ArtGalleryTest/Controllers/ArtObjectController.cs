﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDTO.EntitiesDTO;

namespace ArtGalleryTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArtObjectController : ControllerBase
    {
        private IArtObjectService _artObjectService;
        public ArtObjectController(IArtObjectService artObjectService)
        {
            this._artObjectService = artObjectService;  
        }
      /*  // GET: api/ArtObject
        [HttpGet]
        public  async  Task<IActionResult> Get()
        {
            try
            {
                var data = await _artObjectService.GetAll();
                return Ok(data);
            } catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/ArtObject/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var data = await _artObjectService.GetById(id);
                return Ok(data);
            } catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // POST: api/ArtObject
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ArtObjectDTO artobject)
        {
            try
            {
                var data = await _artObjectService.Add(artobject);
                return Ok(data);
            } catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // PUT: api/ArtObject/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ArtObjectDTO artobject)
        {
            if (id == artobject.Id)
            {
                _artObjectService.Update(artobject);
                return Ok(id);
            } else
            {
                return BadRequest(id);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var data = _artObjectService.GetById(id);
            if (data != null)
            {
               await _artObjectService.Remove(id);
                return Ok("removed");
            } else
            {
                return BadRequest("Looss");
            } */
        }
    }

