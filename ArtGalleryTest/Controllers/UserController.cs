﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDTO.EntitiesDTO;
using ArtGalleryDTO.InterfacesDTO;
using Microsoft.Extensions.Logging;

namespace ArtGalleryTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private  IUserService  _userService;
        
        public UserController(IUserService userService)
        {
            this._userService = userService;
            
        }
        

       
        // GET: api/User
        [HttpGet]
        public async  Task<IActionResult> Get()
        {
            try
            {
                var data = await _userService.GetAll();
                return Ok(data);
            } catch (Exception ex)
            {
                return BadRequest(ex);

            }
            
        }

        // GET: api/User/5
        [HttpGet("{id}", Name = "Get")]
        public async  Task<IActionResult> Get(int id)
        {
            try
            {
                var data = await _userService.GetById(id);
                return Ok(data);
            } catch (Exception ex)
            {
                return BadRequest(ex);            }
        }

        // POST: api/User
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserDTO user)
        {
            try
            {
                var data =await  _userService.Add(user);
                return Ok(data);
               
            } catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // PUT: api/User/5
        [HttpPut("{id}", Name = "PUT")]
        public IActionResult Put(int id, [FromBody] UserDTO user)
        {
           if(id == user.Id) { 
                _userService.Update(user);
                return Ok(user);
            }
            return BadRequest();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult>Delete(int id)
        {
            var data =  await _userService.GetById(id);
            if (data == null)
            {
                return BadRequest(id);
            }  else
            {
                return Ok(_userService.Remove(id));
            }

        }
    }
}
