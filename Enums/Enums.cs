﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enums
{
    public enum Types
    {
        Painting,
        Sculpture,
        Fabric,
        Other,
    }
}
